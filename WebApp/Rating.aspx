﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Rating.aspx.cs" Inherits="WebApp.Rating" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style>
        .starRating {
            width: 50px;
            height: 20px;
            cursor: pointer;
            background-repeat: no-repeat;
            display: block;
        }
        .filledStars {
            background-image: url("images/ratingStarFilled.png")
        }
        .waitingStars {
            background-image: url("images/ratingStarSaved.png")
        }
         .emptyStars {
            background-image: url("images/ratingStarEmpty.png")
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <ajaxToolkit:Rating ID="Rating1" runat="server"
                    StarCssClass="starRating" 
                    FilledStarCssClass="filledStars" 
                    EmptyStarCssClass="emptyStars"
                    WaitingStarCssClass="waitingStars"
                    MaxRating="10"
                    CurrentRating="2"
                    >
                    
                </ajaxToolkit:Rating>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </div>
    </form>
</body>
</html>

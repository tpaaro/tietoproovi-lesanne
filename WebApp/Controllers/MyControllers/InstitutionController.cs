﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using DAL;
using Domain;
using PagedList;

namespace WebApp.Controllers.MyControllers
{
    public class InstitutionController : Controller
    {
        private readonly DataBaseContext _db = new DataBaseContext();

        // GET: Institution
        public ActionResult Index(string sortOrder, string filter, string currentFilter, int? page)
        {
            var institutions = _db.Institutions.Include(i => i.Category);
      
            #region Calculation of 'Average rating' and 'Times rated'
            var feedbacks = _db.Feedbacks.Include(f => f.Institution);
            foreach (var institution in institutions)
            {
                institution.TimesRated = _db.Feedbacks.Count(a => a.InstitutionId == institution.InstitutionId);
                foreach (var feedback in feedbacks)
                {
                    if (feedback.InstitutionId == institution.InstitutionId)
                    {
                        institution.AverageRating += feedback.Rating;
                    }
                }
                
                institution.AverageRating = institution.AverageRating.Equals(0) ? 0 
                    : Math.Round((double)(institution.AverageRating / institution.TimesRated), 0, MidpointRounding.AwayFromZero);
            }
            #endregion
            #region Sorting
            ViewBag.NameSortParm = sortOrder == "name" ? "name_desc" : "name";
            ViewBag.AddressSortParm = sortOrder == "address" ? "address_desc" : "address";
            ViewBag.AvrgParm = sortOrder == "avrg_desc" ? "avrg" : "avrg_desc";
            ViewBag.TimesRatedParm = sortOrder == "times" ? "times_desc" : "times";
            ViewBag.CatParm = sortOrder == "cat_desc" ? "cat" : "cat_desc";
            
            switch (sortOrder)
            {
                case "name":
                    institutions = institutions.OrderBy(s => s.Name);
                    break;
                case "name_desc":
                    institutions = institutions.OrderByDescending(s => s.Name);
                    break;
                case "address":
                    institutions = institutions.OrderBy(s => s.Address);
                    break;
                case "address_desc":
                    institutions = institutions.OrderByDescending(s => s.Address);
                    break;
                // OrderBy not working with numbers.
                case "avrg":
                    institutions = institutions.OrderBy(s => s.AverageRating);
                    break;
                case "avrg_desc":
                    institutions = institutions.OrderByDescending(s => s.AverageRating);
                    break;
                case "times":
                    institutions = institutions.OrderBy(s => s.TimesRated);
                    break;
                case "times_desc":
                    institutions = institutions.OrderByDescending(s => s.TimesRated);
                    break;
                case "cat":
                    institutions = institutions.OrderBy(s => s.Category.Name);
                    break;
                case "cat_desc":
                    institutions = institutions.OrderByDescending(s => s.Category.Name);
                    break;
                default:
                    institutions = institutions.OrderBy(s => s.Category.Name);
                    break;
            }
            #endregion
            #region Paging
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentFilter = filter;
            const int pageSize = 10;
            int pageNumber = (page ?? 1);
            if (filter != null)
            {
                page = 1;
            }
            else
            {
                filter = currentFilter;
            }
            #endregion
            #region Filtering
            if (!String.IsNullOrEmpty(filter))
            {
                var result = institutions.Where(a => a.Name.ToUpper().Contains(filter.ToUpper()));
                return View(result.ToPagedList(pageNumber, pageSize));
            }
            #endregion

            return View(institutions.ToPagedList(pageNumber, pageSize));
            
        }

        // GET: Institution/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Institution institution = _db.Institutions.Find(id);
            if (institution == null)
            {
                return HttpNotFound();
            }
            return View(institution);
        }

        // GET: Institution/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(_db.Categories, "CategoryId", "Name");
            return View();
        }

        // POST: Institution/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InstitutionId,Name,Address,AverageRating,TimesRated,CategoryId")] Institution institution)
        {
            if (ModelState.IsValid)
            {
                _db.Institutions.Add(institution);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(_db.Categories, "CategoryId", "Name", institution.CategoryId);
            return View(institution);
        }

        // GET: Institution/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Institution institution = _db.Institutions.Find(id);
            if (institution == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(_db.Categories, "CategoryId", "Name", institution.CategoryId);
            return View(institution);
        }

        // POST: Institution/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InstitutionId,Name,Address,AverageRating,TimesRated,CategoryId")] Institution institution)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(institution).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(_db.Categories, "CategoryId", "Name", institution.CategoryId);
            return View(institution);
        }

        // GET: Institution/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Institution institution = _db.Institutions.Find(id);
            if (institution == null)
            {
                return HttpNotFound();
            }
            return View(institution);
        }

        // POST: Institution/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Institution institution = _db.Institutions.Find(id);
            _db.Institutions.Remove(institution);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

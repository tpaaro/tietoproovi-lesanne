﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Feedback
    {
        public int FeedbackId { get; set; }
        public String Author { get; set; }
        public String Comment { get; set; }
        public int Rating { get; set; }
        
        public int InstitutionId { get; set; }
        public virtual Institution Institution { get; set; }
    }


}



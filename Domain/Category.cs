﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Category
    {
        public int CategoryId { get; set; }
        public String Name { get; set; }

        public ICollection<Institution> Institutions { get; set; }
    }
}

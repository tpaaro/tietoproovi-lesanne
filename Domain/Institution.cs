﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Institution
    {
        public int InstitutionId { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
        public double AverageRating { get; set; }
        public int? TimesRated { get; set; }

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public ICollection<Feedback> Feedbacks { get; set; }
    }
}
